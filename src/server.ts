import * as config from "config";
import {Application} from "./Application";
import {DateController} from "./controllers/DateController";
import {Startup} from "./startup/Startup";

const context = {app: "template-rest"};
new Startup(context).run();

const app = new Application([new DateController()], config.get("port"));

app.listen();
