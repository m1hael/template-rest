import * as config from "config";
import {Runnable} from "./Runnable";

export class ConfigCheck implements Runnable {
    public run(context?: Object): void {
        if (!config.get("port")) 
            throw new Error("Port configuration is missing.");
        
    }
}
