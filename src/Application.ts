import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as compression from "compression";
import * as cors from "cors";
import * as path from "path";
import * as rfs from "rotating-file-stream";
import * as config from "config";
import * as morgan from "morgan";
import * as swagger from "swagger-jsdoc-express";
import * as swaggerDefinition from "./docs/swagger.json";
import * as winston from "winston";
import globalErrorHandler from "./middleware/errorhandler";

export class Application {
    public app: express.Application;
    public port: number;

    constructor(controllers, port) {
        this.app = express();
        this.port = port;

        this.configureLogging();
        this.configureSwagger();
        this.initializeMiddlewares();
        this.initializeControllers(controllers);
    }

    private configureLogging() {
        // create a rotating write stream
        var accessLogStream = rfs.createStream(
            path.posix.basename(config.get("log") + "/access.log"),
            {
                interval: "1d", // rotate daily
                path: path.dirname(config.get("log") + "/access.log")
            }
        );
        this.app.use(morgan("combined", {stream: accessLogStream}));
        this.app.use(morgan("combined"));
    }

    private initializeMiddlewares() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.text());

        // http
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: true}));
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(cors());
    }

    private initializeControllers(controllers) {
        controllers.forEach(controller => {
            this.app.use("/api", controller.router);
        });

        this.app.use(globalErrorHandler);
    }

    public configureSwagger() {
        // create a '/swagger' endpoint ...
        swagger.setupSwaggerUIFromSourceFiles(
            swaggerDefinition as swagger.SetupSwaggerUIFromSourceFilesOptions,

            // ... and directly register it in 'app'
            this.app
        );
    }

    public listen() {
        this.app.listen(this.port, () => {
            winston.info(`App listening on the port ${this.port}`);
        });
    }
}
