import * as winston from "winston";
import {NextFunction, Request, Response} from "express";

function globalErrorHandler(
    error: Error,
    request: Request,
    response: Response,
    next: NextFunction
) {
    winston.error(error.message);
    response.status(500).send("Internal Server Error");
}

export default globalErrorHandler;
