import * as express from "express";
import * as moment from "moment";

export class DateController {
    public router = express.Router();

    constructor() {
        this.intializeRoutes();
    }

    public intializeRoutes() {
        /**
         * @swaggerPath
         *
         * /date:
         *   get:
         *     tags:
         *       - application
         *     summary: Returns date (default format is ISO)
         *     parameters:
         *       - name: format
         *         description: Date format
         *         in: query
         *         required: false
         *         type: string
         *     produces:
         *       - text/plain
         *     responses:
         *       "200":
         *         description: Successful output of the formatted date
         */
        this.router.get("/date", this.formatDate);
    }

    formatDate = async (
        request: express.Request,
        response: express.Response,
        next: express.NextFunction
    ) => {
        let format = "yyyy-MM-dd";
        
        if (request.query.format) format = request.query.format;

        try {
            return response.status(200).send(moment().format(format));
        } catch (error) {
            next(error);
        }
    };
}
